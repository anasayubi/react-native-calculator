# React Native Calculator

This is simple immitation app that I made of the [Xiaomi calculator application](https://play.google.com/store/apps/details?id=com.miui.calculator). This isn't a complete immitation as I've left some functionality out.

The purpose of creating this was to understand how react and react native works and to create a sample application with them.

`next.txt` - the file that mentions all of the functionality created and all the functionality next for implementation.

## Running the Application

The entire application runs on [Expo](https://expo.dev/).

Steps to run the application:

1. Clone the repository
2. Navigate inside the folder cloned
3. Download all of the node modules
4. `$ npm start`

## Building an APK

Follow the steps mentioned on this [video](https://www.youtube.com/watch?v=7OOE4rQf7zI).

## In-app Screenshots

<img src="./assets/app-screenshots/screenshot1.jpg" width="200">

<img src="./assets/app-screenshots/screenshot2.jpg" width="200">
