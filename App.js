import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  StatusBar,
  Pressable,
  Image,
  Animated,
} from "react-native";

import React from "react";

const windowHeight = Dimensions.get("window").height;
// const screenHeight = Dimensions.get("screen").height;
// const statusBarHeight = StatusBar.currentHeight;
// const navBarHeight = screenHeight - windowHeight - statusBarHeight;

// Key Type enums - all of the type of keys over which
// appropriate styling will be done
const numeric = Symbol("numeric");
const symbol = Symbol("symbol");
const equalSign = Symbol("equalSign");
const AC = Symbol("AC");
const backspace = Symbol("backspace");

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // The string shown on screen that shows the user what sort of
      //   calculation is being done
      expressionString: "",
      // The string that is a interpretation of "expressionString" where
      //   it can be evaluated in the JS runtime - through eval()
      expressionToEval: "",
      // The result string of an evaluation of expression
      expressionResult: "0",
      // Initial size of result text - 35
      resultTextSize: new Animated.Value(35),
      // Initial size of expression text - 60
      expressionTextSize: new Animated.Value(60),
      // Initial color of result text - dim grey
      resultTextColor: new Animated.Value(0),
      // Initial color of expression text - black
      expressionTextColor: new Animated.Value(1),
      // Is the previous key an equals sign?
      prevKeyIsEquals: false,
    };
  }

  // Highlight the result text compared to expression text - increase
  //   the size of result and make it black as well
  highlightResultAnim() {
    Animated.parallel([
      Animated.timing(this.state.resultTextSize, {
        toValue: 60,
        duration: 300,
        useNativeDriver: false,
      }),
      Animated.timing(this.state.expressionTextSize, {
        toValue: 35,
        duration: 300,
        useNativeDriver: false,
      }),
      Animated.timing(this.state.resultTextColor, {
        toValue: 1,
        duration: 300,
        useNativeDriver: false,
      }),
      Animated.timing(this.state.expressionTextColor, {
        toValue: 0,
        duration: 300,
        useNativeDriver: false,
      }),
    ]).start();
  }

  // Highlight the expression text compared to result text - increase
  //   the size of expression and make it black as well
  highlightExprAnim() {
    Animated.parallel([
      Animated.timing(this.state.resultTextSize, {
        toValue: 35,
        duration: 0,
        useNativeDriver: false,
      }),
      Animated.timing(this.state.expressionTextSize, {
        toValue: 60,
        duration: 0,
        useNativeDriver: false,
      }),
      Animated.timing(this.state.resultTextColor, {
        toValue: 0,
        duration: 0,
        useNativeDriver: false,
      }),
      Animated.timing(this.state.expressionTextColor, {
        toValue: 1,
        duration: 0,
        useNativeDriver: false,
      }),
    ]).start();
  }

  handleKeyClick(value, charDisplayed) {
    console.log("handleKeyClick");

    const newExpressionEval = this.state.expressionToEval + value;
    const newExprEvalForDisplay = this.state.expressionString + charDisplayed;

    // Flag in case operational symbol is the current char added
    let charAddedIsSymbol = false;
    if (value === "+" || value === "-" || value === "*" || value === "/")
      charAddedIsSymbol = true;

    // Flag in case the last char of expressionToVal
    //   (previous to current value) is an operational symbol
    let prevCharIsSymbol = false;
    let prevChar = this.state.expressionToEval.substring(
      this.state.expressionToEval.length - 1,
      this.state.expressionToEval.length
    );
    if (
      prevChar === "+" ||
      prevChar === "-" ||
      prevChar === "*" ||
      prevChar === "/"
    ) {
      prevCharIsSymbol = true;
    }

    // Assume that first char of expression is an operation symbol.
    //   In that case show "0+" in case "+" is clicked
    if (this.state.expressionToEval === "" && charAddedIsSymbol) {
      console.log("1");

      this.setState({
        expressionString: "0" + charDisplayed,
        expressionToEval: "0" + value,
        expressionResult: "= 0",
      });
    }
    // In case an operational symbol is repeated then replace the prev
    //   operational symbol and with the new one added by user
    else if (charAddedIsSymbol && prevCharIsSymbol) {
      console.log("2");

      const replaceSymbolExp =
        this.state.expressionToEval.substring(
          0,
          this.state.expressionToEval.length - 1
        ) + value;

      const replaceSymbolForDisplayExp =
        this.state.expressionString.substring(
          0,
          this.state.expressionString.length - 1
        ) + charDisplayed;

      this.setState({
        expressionString: replaceSymbolForDisplayExp,
        expressionToEval: replaceSymbolExp,
      });
    }
    // Start a new expression once equals sign has been clicked and
    //   a character is mentioned
    else if (this.state.prevKeyIsEquals && !charAddedIsSymbol) {
      console.log("3");

      this.setState({
        expressionString: charDisplayed,
        expressionToEval: value,
        expressionResult: "= " + charDisplayed,
        // After execution equal sign is no longer prev char pressed
        prevKeyIsEquals: false,
      });

      this.highlightExprAnim();
    }
    // Use the results of the previous expression if a symbol is clicked
    else if (this.state.prevKeyIsEquals && charAddedIsSymbol) {
      console.log("4");

      const resultStr = this.state.expressionResult.split(" ")[1];
      // console.log(resultStr);

      this.setState({
        expressionString: resultStr + charDisplayed,
        expressionToEval: resultStr + value,
        expressionResult: "= " + resultStr,
        // After execution equal sign is no longer prev char pressed
        prevKeyIsEquals: false,
      });

      this.highlightExprAnim();
    }
    // Otherwise treat it normally
    else {
      console.log("5");

      this.setState({
        expressionString: newExprEvalForDisplay,
        expressionToEval: newExpressionEval,
      });

      if (value !== "/" && value !== "*" && value !== "+" && value !== "-") {
        this.setState({
          expressionResult: "= " + String(eval(newExpressionEval)),
        });
      }
    }
  }

  handleClearClick() {
    console.log("handleClearClick");

    this.setState({
      expressionString: "",
      expressionToEval: "",
      expressionResult: "0",
    });
  }

  handleDeleteClick() {
    console.log("handleDeleteClick");

    // Do nothing if prev key is equals
    if (this.state.prevKeyIsEquals) {
      console.log("3");

      return;
    }

    const newExpressionStr = this.state.expressionString.substring(
      0,
      this.state.expressionString.length - 1
    );

    const newExpressionEval = this.state.expressionToEval.substring(
      0,
      this.state.expressionToEval.length - 1
    );

    this.setState({
      expressionString: newExpressionStr,
      expressionToEval: newExpressionEval,
    });

    // The last character of the new expression
    const lastCharOfNewExpr = newExpressionEval.substring(
      newExpressionEval.length - 1,
      newExpressionEval.length
    );

    // Find out if the last character is an operation symbol
    //   If it is the expression can't be evaluated and we should
    //   remove more characters until the expression becomes valid
    //   and evaluate that
    if (
      lastCharOfNewExpr === "/" ||
      lastCharOfNewExpr === "*" ||
      lastCharOfNewExpr === "+" ||
      lastCharOfNewExpr === "-"
    ) {
      console.log("1");

      // The previous expression is invalid so remove the last char
      //   so that it becomes valid
      const validExpressionEval = newExpressionEval.substring(
        0,
        newExpressionEval.length - 1
      );

      this.setState({
        expressionResult: "= " + String(eval(validExpressionEval)),
      });
    }
    // If the expression after char deletion is an empty string -
    //   output a 0
    else if (newExpressionEval === "") {
      console.log("2");

      this.setState({
        expressionResult: "0",
      });
    }
    // In case the expression is not invalid - continue like normal and
    //   and just evaluate the current expression (after character
    //   deletion)
    else {
      console.log("3");

      this.setState({
        expressionResult: "= " + String(eval(newExpressionEval)),
      });
    }
  }

  handleEqualsClick(c) {
    console.log("handleEqualsClick");

    this.highlightResultAnim();

    this.setState({
      prevKeyIsEquals: true,
    });
  }

  handleDotClick(c) {
    console.log("handleDotClick");

    // If dot is the first char then insert "0.", otherwise proceed to all
    //   other cases
    if (this.state.expressionToEval === "") {
      this.setState({
        expressionString: "0.",
        expressionToEval: "0.",
        expressionResult: "= 0",
      });

      return;
    }

    // The regular expression to split a string between operators
    // ?: to disable the capturing group - the operators don't come up
    //   after the split. Only the numbers come up
    const re = /(?:[\*\/\-\+])/;
    // Extract array of only numbers
    const numbers = this.state.expressionToEval.split(re);
    // Once empty strings have been removed
    const numbersWithoutEmpty = helperRemoveEmptyStringsFromArray(numbers);
    // Find out if the last number has a dot
    const lastNumber = numbersWithoutEmpty[numbersWithoutEmpty.length - 1];
    const doesLastNumberHaveDot = lastNumber.includes(".");

    if (doesLastNumberHaveDot) {
    } else {
      this.setState({
        expressionString: this.state.expressionString + c,
        expressionToEval: this.state.expressionToEval + c,
      });
    }
  }

  // componentDidUpdate() {
  //   console.log(this.state.expressionToEval);
  //   console.log(this.state.expressionString);
  // }

  render() {
    return (
      <View>
        <Expression
          expression={this.state.expressionString}
          result={this.state.expressionResult}
          resultTextSize={this.state.resultTextSize}
          expressionTextSize={this.state.expressionTextSize}
          resultTextColor={this.state.resultTextColor}
          expressionTextColor={this.state.expressionTextColor}
        />
        <Keyboard
          handleKeyClick={(v, d) => this.handleKeyClick(v, d)}
          handleClearClick={() => this.handleClearClick()}
          handleDeleteClick={() => this.handleDeleteClick()}
          handleEqualsClick={(c) => this.handleEqualsClick(c)}
          handleDotClick={(c) => this.handleDotClick(c)}
        />
      </View>
    );
  }
}

class Keyboard extends React.Component {
  styles = StyleSheet.create({
    keyboard: {
      maxHeight: 500,
      flexWrap: "wrap",
      justifyContent: "flex-end",
    },
  });

  render() {
    return (
      <View style={this.styles.keyboard}>
        <Key
          value="AC"
          onClick={() => this.props.handleClearClick()}
          type={AC}
        />
        {/* v is value ('*' & '/' for multiplication and division) 
        & d is charDisplayed ('×' & '÷' for multiplication and division) */}
        <Key
          value="7"
          onClick={(v, d) => this.props.handleKeyClick(v, d)}
          type={numeric}
        />
        <Key
          value="4"
          onClick={(v, d) => this.props.handleKeyClick(v, d)}
          type={numeric}
        />
        <Key
          value="1"
          onClick={(v, d) => this.props.handleKeyClick(v, d)}
          type={numeric}
        />
        <Key value="" />

        <Key
          value="B"
          onClick={() => this.props.handleDeleteClick()}
          type={backspace}
        />
        <Key
          value="8"
          onClick={(v, d) => this.props.handleKeyClick(v, d)}
          type={numeric}
        />
        <Key
          value="5"
          onClick={(v, d) => this.props.handleKeyClick(v, d)}
          type={numeric}
        />
        <Key
          value="2"
          onClick={(v, d) => this.props.handleKeyClick(v, d)}
          type={numeric}
        />
        <Key
          value="0"
          onClick={(v, d) => this.props.handleKeyClick(v, d)}
          type={numeric}
        />

        <Key value="" />
        <Key
          value="9"
          onClick={(v, d) => this.props.handleKeyClick(v, d)}
          type={numeric}
        />
        <Key
          value="6"
          onClick={(v, d) => this.props.handleKeyClick(v, d)}
          type={numeric}
        />
        <Key
          value="3"
          onClick={(v, d) => this.props.handleKeyClick(v, d)}
          type={numeric}
        />
        <Key
          value="."
          onClick={(c) => this.props.handleDotClick(c)}
          type={numeric}
        />

        <Key
          value="/"
          symbol="÷"
          onClick={(v, d) => this.props.handleKeyClick(v, d)}
          type={symbol}
        />
        <Key
          value="*"
          symbol="×"
          onClick={(v, d) => this.props.handleKeyClick(v, d)}
          type={symbol}
        />
        <Key
          value="-"
          onClick={(v, d) => this.props.handleKeyClick(v, d)}
          type={symbol}
        />
        <Key
          value="+"
          onClick={(v, d) => this.props.handleKeyClick(v, d)}
          type={symbol}
        />
        <Key
          value="="
          onClick={(c) => this.props.handleEqualsClick(c)}
          type={equalSign}
        />
      </View>
    );
  }
}

class Expression extends React.Component {
  styles = StyleSheet.create({
    expression: {
      marginTop: StatusBar.currentHeight,
      height: windowHeight - 500,
      backgroundColor: "white",
      // flexDirection: "column-reverse",
      justifyContent: "flex-end",
      alignItems: "flex-end",
      marginRight: 20,
    },
  });

  render() {
    return (
      <View style={this.styles.expression}>
        <Animated.Text
          style={{
            fontSize: this.props.expressionTextSize,
            color: this.props.expressionTextColor.interpolate({
              inputRange: [0, 1],
              outputRange: ["rgb(105,105,105)", "rgb(0, 0, 0)"],
            }),
          }}
        >
          {this.props.expression}
        </Animated.Text>
        <Animated.Text
          style={{
            fontSize: this.props.resultTextSize,
            color: this.props.resultTextColor.interpolate({
              inputRange: [0, 1],
              outputRange: ["rgb(105,105,105)", "rgb(0, 0, 0)"],
            }),
          }}
        >
          {this.props.result}
        </Animated.Text>
      </View>
    );
  }
}

class Key extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: this.props.value,
      symbol: this.props.symbol,
      type: this.props.type,
    };
  }

  styles = StyleSheet.create({
    key: {
      width: 100,
      height: 100,
      // backgroundColor: "white",
      // borderColor: "black",
      // borderWidth: 2,
      alignItems: "center",
      justifyContent: "center",
    },
    keyTextNumeric: {
      color: "#000000",
      fontSize: 35,
    },
    keyTextSymbols: {
      color: "#F08638",
      fontSize: 38,
    },
    keyTextEqualSign: {},
    keyTextAC: {
      color: "#F08638",
      fontSize: 28,
      fontWeight: "600",
    },
    keyTextBackspace: {},
  });

  render() {
    // What should be displayed on a button and shown in expression
    // In case of multipication - the * character must be evaluated and × must be shown
    const charDisplayed = this.state.symbol
      ? this.state.symbol
      : this.state.value;

    // Decide the styles of the key based on the type of key
    let styles;
    switch (this.state.type) {
      case numeric:
        styles = this.styles.keyTextNumeric;
        break;
      case symbol:
        styles = this.styles.keyTextSymbols;
        break;
      case equalSign:
        styles = this.styles.keyTextEqualSign;
        break;
      case AC:
        styles = this.styles.keyTextAC;
        break;
      case backspace:
        styles = this.styles.keyTextBackspace;
        break;
    }

    // Decide if an image should be added for the backspace button
    let key;
    if (this.state.type === backspace) {
      key = (
        <View style={this.styles.key}>
          <Image
            source={require("./assets/backspace-icon.png")}
            style={{ height: 35, resizeMode: "contain" }}
          />
        </View>
      );
    } else if (this.state.type === equalSign) {
      key = (
        <View style={this.styles.key}>
          <View
            style={{
              width: 60,
              height: 60,
              borderRadius: 30,
              backgroundColor: "#F08638",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Text style={{ color: "#FFFFFF", fontSize: 40 }}>
              {charDisplayed}
            </Text>
          </View>
        </View>
      );
    } else {
      key = (
        <View style={this.styles.key}>
          <Text style={styles}>{charDisplayed}</Text>
        </View>
      );
    }

    if (this.state.value) {
      return (
        <Pressable
          onPressIn={() => this.props.onClick(this.state.value, charDisplayed)}
        >
          {key}
        </Pressable>
      );
    } else {
      return <>{key}</>;
    }
  }
}

//
function helperRemoveEmptyStringsFromArray(a) {
  return a.filter((e) => {
    if (e !== "") return true;
    else return false;
  });
}
